package chess;

/**
 * @author Jean Moscoso
 * @author Greyson Frazier
 * 
 */
public class Pawn{
	
	char color;
	int code = -1;
	int posX = 0;
	int posY = 0;
	
	public Pawn(char color, int x, int y) {
		this.color = color;
		
		if(color == 'b') {
			 posX = x;
			 posY =	y;	 
			 code = 11;
		}
		else if(color == 'w') {
			posX = x;
			posY = y;
			code = 5;
		}
		
		
		
	}
	
	public int getCode() {
		
		return this.code;
	}

	
	public int getPosX() {
		
		return this.posX;
	}
	
    public int getPosY() {
		
		return this.posY;
	}
	
	
	
	
	public boolean validMovement(int code, int fromX, int fromY, int toX, int toY) {	

		int distY = toY - fromY;
		int distX = Math.abs(toX - fromX); //Should be 0 most of the time, 1 for attacking
	
		if(code == 5) {
		
		
			if(distY == -2 && fromY == 6 && distX == 0)
			{
				
				
				return true;
			}
			else if(distY == -1 && distX == 1 && (Board.board[toX][toY] != 12 && Board.board[toX][toY] !=13))
			{
				
				return true;
			}
			else if(distY == -1 && distX == 0)
			{
				
			
				return true;
			}
			else if(Board.piece == 11 && Board.lastDistX == 0 && Board.lastDistY == 2 && fromY == 3)
			{
				//ERASE PAWN CODE HERE
		
				Board.board[Board.lastX][Board.lastY] = 12;
			
				if(Board.lastY%2 == 0 && Board.lastX %2 != 0) {
					Board.board[Board.lastX][Board.lastY] = 13;
				} else if(Board.lastY%2 != 0 &&  Board.lastX%2 == 0){
					Board.board[Board.lastX][Board.lastY] = 13;
				}
				return true;
			}
		
		} else if(code == 11) {
			
			if(distY == 2 && fromY == 1 && distX == 0)
			{
				
				
				return true;
			}
			else if(distY == 1 && distX == 1 && (Board.board[toX][toY] != 12 && Board.board[toX][toY] != 13))
			{
				
				return true;
			}
			else if(distY == 1 && distX == 0)
			{
				
			
				return true;
			}
			else if(Board.piece == 5 && Board.lastDistX == 0 && Board.lastDistY == 2 && fromY == 4)
			{
				//ERASE PAWN CODE HERE
				
				Board.board[Board.lastX][Board.lastY] = 12;
				
				if(Board.lastY%2 == 0 && Board.lastX %2 != 0) {
					Board.board[Board.lastX][Board.lastY] = 13;
				} else if(Board.lastY%2 != 0 &&  Board.lastX%2 == 0){
					Board.board[Board.lastX][Board.lastY] = 13;
				}
				return true;
			}
			
			
			
		}
		
	
		return false;
	}

	
}