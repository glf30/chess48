package pieces;

/**
 * @author Jean Moscoso
 * @author Greyson Frazier
 * 
 */

/**
 * Interface for all advanced (non-Pawn) pieces
 */
public interface PieceInterface {

    /**
     * Returns the integer code of the specified piece
     * @return
     */
    public int getCode();

    /**
     * Returns the x position of the specified piece
     * @return
     */
	public int getPosX();

    /**
     * Returns the y position of the specified
     * @return
     */
    public int getPosY();

    /**
     * Checks if the specified coordinates yield a valid movement
     * @param fromX
     * @param fromY
     * @param toX
     * @param toY
     * @return
     */
	public boolean validMovement(int fromX, int fromY, int toX, int toY);
	
	
	
}